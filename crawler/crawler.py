import os
import sys
import yaml
import json
import requests
import pandas as pd
import re
from sqlalchemy import create_engine

PROJECT_NAME = 'torob/'
ROOT_DIR = str(str(os.path.realpath(__file__).replace('\\', '/')).split(PROJECT_NAME)[0]) + PROJECT_NAME
sys.path.append(ROOT_DIR)

with open(ROOT_DIR + "crawler/crawler_configs/general_configuration.yaml", 'r') as f:
    gen_conf = yaml.safe_load(f)


class Crawler:
    def __init__(self, size):
        self.url = gen_conf['main_url'].format('{}', size)
        self.products_df = pd.DataFrame(columns=['local_slug', 'name', 'brand', 'capacity', 'store', 'price'])

    def card_crawler(self):
        page = 0
        while True:
            sys.stdout.write("\rCurrent page: %i" % page)
            sys.stdout.flush()
            a = requests.get(self.url.format(page))
            card_results = json.loads(a.text)['results']
            if not card_results:
                self.fix_data_type()
                self.transfer_data()
            else:
                series_card = pd.DataFrame(card_results).more_info_url
                for product in series_card.items():
                    self.single_crawler(product[1])
            page += 1
            self.fix_data_type()
            self.transfer_data()
            break

    def single_crawler(self, url):
        product_result = json.loads(requests.get(url).text)
        capacity = self.get_json_value(product_result, 'structural_specs', 'headers', 0, 'specs', 'ظرفیت')
        brand = self.get_json_value(product_result, 'attributes', 'brand')
        local_key = self.get_json_value(product_result, 'random_key')
        product_name = self.get_json_value(product_result, 'name2')
        stored_list = [local_key, product_name, brand, capacity]
        try:
            store_price = pd.DataFrame(product_result['products_info']['result'])[['shop_name', 'price']]
            store_price = store_price[store_price.price != 0].sort_values('price')
            stored_list += store_price.iloc[0].values.tolist()
            self.products_df.loc[self.products_df.shape[0]] = stored_list
        except:
            pass

    def fix_data_type(self):
        df_droped = self.products_df.dropna(subset=['capacity'])
        df_droped.loc[df_droped.capacity.str.contains('ترابایت'), 'capacity'] = \
            df_droped[df_droped.capacity.str.contains('ترابایت')].capacity.str.replace('ترابایت', '')

        df_droped.loc[df_droped.capacity.str.contains('دو'), 'capacity'] = \
            df_droped[df_droped.capacity.str.contains('دو')].capacity.str.replace('دو', '2')

        df_droped.loc[df_droped.capacity.str.contains('پنج'), 'capacity'] = \
            df_droped[df_droped.capacity.str.contains('پنج')].capacity.str.replace('پنج', '5')

        df_droped.loc[df_droped.capacity.str.contains('چهار'), 'capacity'] = \
            df_droped[df_droped.capacity.str.contains('چهار')].capacity.str.replace('چهار', '4')

        df_droped.loc[df_droped.capacity.str.contains('یک'), 'capacity'] = \
            df_droped[df_droped.capacity.str.contains('یک')].capacity.str.replace('یک', '1')

        df_droped.loc[:, 'capacity'] = df_droped.loc[:, 'capacity'].apply(
            lambda x: int(re.search(r'\d+', x).group())
        )
        self.products_df = df_droped.copy()

    def transfer_data(self):
        eng = self.make_mysql_db_connection_alchemy('local_host')
        self.products_df.to_sql('hard_driver', con=eng, if_exists='append', index=False)

    @staticmethod
    def get_json_value(json_input, *argv):
        value = json_input
        for arg in argv:
            try:
                value = value[arg]
            except:
                return None
        return value

    @staticmethod
    def make_mysql_db_connection_alchemy(server: str):
        user = gen_conf[server]['user_name']
        password = gen_conf[server]['password']
        dbname = gen_conf[server]['db_name']
        engine = f"postgresql://{user}:{password}@localhost/{dbname}"
        return engine
