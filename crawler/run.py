import sys
import os

PROJECT_NAME = 'torob/'
ROOT_DIR = str(str(os.path.realpath(__file__).replace('\\', '/')).split(PROJECT_NAME)[0]) + PROJECT_NAME
sys.path.append(ROOT_DIR)

from crawler import Crawler

if __name__ == '__main__':
    a = Crawler(10)
    a.card_crawler()
