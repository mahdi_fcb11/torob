import os
import sys
import yaml

PROJECT_NAME = 'torob/'
ROOT_DIR = str(str(os.path.realpath(__file__).replace('\\', '/')).split(PROJECT_NAME)[0]) + PROJECT_NAME
sys.path.append(ROOT_DIR)

with open(ROOT_DIR + "db_config/db_config.yaml", 'r') as f:
    db_conf = yaml.safe_load(f)


class Config:
    SECRET_KEY = 'secret string'
    SQLALCHEMY_DATABASE_URI = f"postgresql://{db_conf['user_name']}:{db_conf['password']}@localhost/{db_conf['db_name']}"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
