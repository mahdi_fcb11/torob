import sys
import os
import yaml
from flask_sqlalchemy import SQLAlchemy


PROJECT_NAME = 'torob/'
ROOT_DIR = str(str(os.path.realpath(__file__).replace('\\', '/')).split(PROJECT_NAME)[0]) + PROJECT_NAME
sys.path.append(ROOT_DIR)

with open(ROOT_DIR + "db_config/db_config.yaml", 'r') as f:
    db_conf = yaml.safe_load(f)

db = SQLAlchemy()


class hard_driver(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    local_slug = db.Column(db.String(), nullable=True)
    name = db.Column(db.String(), nullable=True)
    brand = db.Column(db.String(), nullable=True)
    capacity = db.Column(db.Integer, nullable=True)
    store = db.Column(db.String(), nullable=True)
    price = db.Column(db.Integer, nullable=True)

    def __init__(self, local_slug, name, brand=None, capacity=None, store=None, price=None):
        self.local_slug = local_slug
        self.name = name
        self.brand = brand
        self.capacity = capacity
        self.store = store
        self.price = price
