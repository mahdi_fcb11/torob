import os
import sys
from flask import Flask, request, Blueprint


PROJECT_NAME = 'torob/'
ROOT_DIR = str(str(os.path.realpath(__file__).replace('\\', '/')).split(PROJECT_NAME)[0]) + PROJECT_NAME
sys.path.append(ROOT_DIR)


from Models import hard_driver

driver_info = Blueprint('hard_drive', __name__,)


@driver_info.route('/driver-info', methods=['GET'])
def hard_drive():
    store = request.args.get('store')
    capacity = request.args.get('capacity')
    min_price = request.args.get('min_price')
    max_price = request.args.get('max_price')
    brand = request.args.get('brand')

    driver = filter_query(hard_driver, hard_driver.query, min_price=min_price, max_price=max_price,
                          store=store, brand=brand, capacity=capacity)
    results = []
    for result in driver:
        results.append({'name': result.name, 'price': result.price, 'store': result.store})
    return {'message': 'succeed', 'result': results}, 200


def filter_query(hard_driver, query, **kwargs):
    for key, value in kwargs.items():
        if value:
            if key == 'store':
                query = query.filter(hard_driver.store.contains(value))
            elif key == 'min_price':
                query = query.filter(hard_driver.price >= value)
            elif key == 'max_price':
                query = query.filter(hard_driver.price <= value)
            else:
                query = query.filter_by(**{key: value})
    return query.all()
