import sys
import os
from flask import Flask

PROJECT_NAME = 'torob/'
ROOT_DIR = str(str(os.path.realpath(__file__).replace('\\', '/')).split(PROJECT_NAME)[0]) + PROJECT_NAME
sys.path.append(ROOT_DIR)

from external_drive.web_service import driver_info
from Models import db
from config import Config


if __name__ == '__main__':
    app = Flask(__name__)
    app.config.from_object(Config)
    db.init_app(app)
    app.register_blueprint(driver_info)
    app.run(host='0.0.0.0')
